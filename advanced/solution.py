#!/usr/bin/env python3

# Un'armoured' (unpadded) RSA is vulnerable to signature forgery
# as the crypto scheme is malleable. Beacuse it's all just numbers
# the signature can have factors removed from it through the modular inverse operation.
#
# The exploit for this challenge is to manipulate the payload we want to sign with a fudge factor,
# ask the attestor to sign the payload, and then remove the fudge factor with modular inverse to
# construct the signature of the payload the attestor refuses to sign.
#
# Flag: yodv9Acac32vO2plL+DzICN1KHFqwcOVT9cU7IpvGz/qBoQTAPDd232ytBJ84fCq1lTnR930SDt3WpqdJESggV3FWwpfs3ZjaAE3NRFN5iB0VwHHL6G1fIhICyMXzQJK501HGJskFUvY3E/nr+VYsCn9qH/OXc7yhzrpmgVtobYKtdbge2uVWZxRiCHOvxzXjhFI7k0oVCp219u5kY/TZPazijVyX6AWfbYSSqu12EB78UdCNoTjOvap4EDwlCk9lIAkESkJVObo3BDTcx2KlOBnqdnMAwKhJb8mr/gqYHhxNkOxYQ+ckC+nManWaJ07pWTpC+3shDTyA9JmggQzt6gOOoCo81CHtg5lXdIG9E4DhOFNvraYTWV7ykUmGwcIRP+RDATMn34jPajYw0OEJ4ycBWfy2g6YR08T47IEdS2TvTixRiusFoVias/TIMmlNMmx49LC5BSnHUG8yratEgBSLIYGNnnYCZ1G6fgrZGXSakjngS28I+q1jFqCaARDKlPbJ8Iwk7/I/DHYOGb5YKCpAmw2lA2kd1lkJUywvFTcqEiR4RoKfblIJPb3OrCzenMpsYwqlh89PQz2o2E8vlDYUe+qd1zheDd1TBVhEltcHB1ph+V3ZW9jM10Yl4uJpk/ZpgdI13nM/2603dRSbPEhbUcJYCVeIQ7PZRE8zAQ=
#
# This solution requires pwntools3 to run,
# install with `pip3 install --upgrade git+https://github.com/arthaud/python3-pwntools.git`

import os
import pwn
import json

from Crypto.PublicKey import RSA
from base64 import b64encode, b64decode

IP = '127.0.0.1'
PORT = 6000


def extended_euclidean(a, b):
    """Extended Euclidean Algorithm"""
    if a == 0:
        return (b, 0, 1)

    g, y, x = extended_euclidean(b % a, a)
    return (g, x - (b // a) * y, y)


def modinv(a, m):
    """Modular Inverse Function"""
    g, x, y = extended_euclidean(a, m)
    return x % m


def attestor_sign(hash):
    exploit = json.dumps({
        'binary_hash': '{:02x}'.format(hash),
        'breakglass': False
    })
    print(f"Sending {exploit}")

    server = pwn.remote(IP, PORT)
    server.sendline(exploit)
    signature = server.recv()
    signature = b64decode(signature.decode('utf-8'))
    return signature


# Get the public key
RSA_PUBLIC_KEY_FILE = os.environ.get("RSA_PUBLIC_KEY_FILE")

with open(RSA_PUBLIC_KEY_FILE) as f:
    public_key = RSA.importKey(f.read())

banned_hash = "783791b5901b2714faeb09b1cf38b824d1209713f4e0034283b0e79df0e21f3c"

# M' = ((S**E mod N) M) mod N
# A nice easy number
fudge_factor = 5
print(f"Fudge factor we chose: {fudge_factor}")

# Craft malicious message with the fudge factor
malicious_message = (pow(fudge_factor, public_key.e, public_key.n) * int(banned_hash, base=16)) % public_key.n
print(f"Malicious message: {malicious_message}")

# Sign the malicious message
malicious_signature = attestor_sign(malicious_message)
print(f"Malicious signature: {malicious_signature}")

# Remove the fudge factor
unfudged_signature = (int.from_bytes(malicious_signature, byteorder='little') * modinv(fudge_factor, public_key.n)) % public_key.n
unfudged_signature = b64encode(unfudged_signature.to_bytes((unfudged_signature.bit_length() + 7) // 8, byteorder='little')).decode('ascii')
print(f"Final signature: {unfudged_signature}")
