# Flag

`6ecd45827ea9222f024389a5e6b684f6e904022fc2c1bbf415cd304ee597`

decrypts to 

`secret: dont_cross_the_streams`

with the following key:

`1da826f01bdd180f662ce7d1b9d5f6999a775d5baaa4e48761bf552f88e41bdfc1836285ae0e88af04227fc0ad785539a786267e4d4fc61c545cf23084a958806fdeb499`.

Therefore the flag for this challenge is `dont_cross_the_streams`

# Explanation

This challenge can be solved through either Crib dragging or statistical analysis, through what is known as a Many Time Pad attack. The following tools will assist but not immediately solve the challenge:
- https://toolbox.lotusfa.com/crib_drag/
- https://github.com/CameronLonsdale/MTP

This challenge will be slightly challenging for someone completely new, but it's made easier by the fact that there is an information leak about what the plaintext is about, and that the task can be subdivided among team mates to decrypt faster.
